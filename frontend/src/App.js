import React, {Component} from 'react';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			inputs: [{modelo:""}],
			owner: "",
			description: ""
		};
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	 componentDidMount(){
		this.fetchDados();
	}

	fetchDados = async ()=>{
		const dados = await fetch("http://localhost:8880/api/testecnm")
		const response = await dados.json();
		this.setState(() => ({
			inputs: response,
		}));

	}
	handleChange = (e) => {
		if (["modelo"].includes(e.target.className) ) {
			let inputs = [...this.state.inputs]
			inputs[e.target.dataset.id][e.target.className] = e.target.value.toUpperCase()
			this.setState({ inputs }, () => console.log(this.state.inputs))
		} else {
			this.setState({ [e.target.name]: e.target.value.toUpperCase() })
		}
	}



	handleSubmit = (e) => { 
		const save = this.state.inputs.map(function(res,i){
			return fetch(`/salvardados/${res.id}/${res.modelo}`)
		});

	}
	
	render() {
		let {inputs} = this.state
		return (
			<React.Fragment>
				<h1>TESTE CHAVESNAMAO</h1>
				<form onSubmit={this.handleSubmit} onChange={this.handleChange} >
					{
						inputs.map((val, idx)=> {
							return (
							<div key={idx}>
								<label htmlFor={inputs[idx].id}>{` Id: ${inputs[idx].id} `}      </label>
								<br/>
								<label htmlFor={inputs[idx].nome}>{` Nome: ${inputs[idx].nome} `}      </label>

								<input
									type="text"
									name={inputs[idx].modelo}
									id={inputs[idx].id}
									data-id={idx}
									defaultValue={inputs[idx].modelo} 
									className="modelo"
								/>
								<hr/>
							</div>

						)
					})
					}
					<input type="submit" value="Submit" /> 
				</form>
		  </React.Fragment>
		)
	  }
	}

export default App;
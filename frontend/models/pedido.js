   module.exports = (sequelize, DataTypes) => {

    var pedido = sequelize.define('pedido', {
        modelo: DataTypes.STRING
    },
      {
        freezeTableName: true,
        createdAt: false,
        updatedAt: false,
        tableName: 'pedido'
      
    });
    return pedido;
  }